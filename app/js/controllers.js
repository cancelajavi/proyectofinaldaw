/*
    Autor = Javier Cancela Torres
    Fecha = 09/06/2016
    Licencia = GPL v3
    Versión = 1.0
    Descripción = Notijuegos, tu página de videojuegos
    Copyright (C) 2015  Javier Cancela Torres
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/* Controllers */

var notiJuegosControllers = angular.module('notiJuegosControllers', []);
//Controlador del estado inicial de la aplicacion
notiJuegosControllers.controller('PostListCtrl', ['$scope', 'notijuegosService','$rootScope',
        function($scope, notijuegosService, $rootScope) {
            $rootScope.logged = false;
            $scope.slides = [{
                image: 'img/slider-1v2.jpg',
                description: 'Image Slider 1'
            }, {
              image: 'img/slider-2.jpg',
              description: 'Image Slider 2'
            }, {
              image: 'img/slider-3.jpg',
              description: 'Image Slider 3'
            }];

            $scope.direction = 'left';
            $scope.currentIndex = 0;

            $scope.setCurrentSlideIndex = function(index) {
                $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
                $scope.currentIndex = index;
            };

            $scope.isCurrentSlideIndex = function(index) {
                return $scope.currentIndex === index;
            };

            $scope.prevSlide = function() {
                $scope.direction = 'left';
                $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
            };

            $scope.nextSlide = function() {
                $scope.direction = 'right';
                $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
            };
            //llamamos al servicio para pedir los artículos
            notijuegosService.get({
                    accion: "pedirPosts",
                },
                function(data) {
                    $scope.posts = data.postsData;

                });
        }
        //animacion del slider
    ]).animation('.slide-animation', function () {
            return {
                beforeAddClass: function (element, className, done) {
                    var scope = element.scope();

                    if (className == 'ng-hide') {
                        var finishPoint = element.parent().width();
                        if(scope.direction !== 'right') {
                            finishPoint = -finishPoint;
                        }
                        TweenMax.to(element, 0.5, {left: finishPoint, onComplete: done });
                    }
                    else {
                        done();
                    }
                },
                removeClass: function (element, className, done) {
                    var scope = element.scope();

                    if (className == 'ng-hide') {
                        element.removeClass('ng-hide');

                        var startPoint = element.parent().width();
                        if(scope.direction === 'right') {
                            startPoint = -startPoint;
                        }

                        TweenMax.fromTo(element, 0.5, { left: startPoint }, {left: 0, onComplete: done });
                    }
                    else {
                        done();
                    }
                }
            };
        })
        //controlador para las noticias
    .controller('NewsDetailsCtrl', ['$scope', '$stateParams', 'notijuegosService',
        function($scope, $stateParams, notijuegosService) {
          //llamamos al servicio para obtener los datos de la noticia
            notijuegosService.get({
                    accion: "detallesPost",
                    postUrl: "'" + $stateParams.postUrl + "'"
                },
                function(data) {
                    $scope.post = data.post[0];
                });

        }
    ])
    //controlador para los análisis
    .controller('ReviewsDetailsCtrl', ['$scope', '$stateParams', 'notijuegosService',
        function($scope, $stateParams, notijuegosService) {
          //Llamamos al servicio para obtener los datos de los análisis
            notijuegosService.get({
                    accion: "detallesPost",
                    postUrl: "'" + $stateParams.postUrl + "'"
                },
                function(data) {
                    $scope.post = data.post[0];
                });

        }
    ])
    //controlador para el menú
    .controller('HeaderCtrl', ['$scope', '$stateParams', 'userSrv','$state',
            function($scope, $stateParams, userSrv, $state) {
              //controlamos si el usuario ha cambiado para pasarselo a la vista $scope.
              $scope.$state = $state;
              $scope.$watch(function () {
                return userSrv.getUser();
              }, initialize, true);

              function initialize() {
                var user = userSrv.getUser();
                $scope.isLogin = user.uid || false;
                $scope.name = user.user_name || "Login";
              }
          }
     ])
     //controlador para la autenticacion del usuario
    .controller('authCtrl',['$scope', '$rootScope', '$routeParams', '$state', '$http', 'Data', 'userSrv', 'cookieUserSrv', function($scope, $rootScope, $routeParams, $state, $http, Data, userSrv, cookieUserSrv) {
        //inicializamos objetos
        $scope.login = {};
        $scope.signup = {};
        $scope.results = userSrv.getUser();
        //funcion para hacer el login
        $scope.doLogin = function(user) {
            Data.post('login', {
                user: user
            }).then(function(results) {
                Data.toast(results);
                if (results.status == "success") {
                    userSrv.setUser(results);
                    cookieUserSrv.setUserCookie(results);
                    $state.go('home.dashboard');
                }
            });
        };
        //funcion para el registro
        $scope.signup = {
            email: '',
            password: '',
            user_name: '',
            surname: '',
            username: ''
        };
        $scope.signUp = function(user) {
            Data.post('signUp', {
                user: user
            }).then(function(results) {
                Data.toast(results);
                if (results.status == "success") {
                    $state.go('home.login');
                }
            });
        }
        //funcion para la desconexion
        $scope.logout = function() {
            Data.get('logout').then(function(results) {
                userSrv.setUser({});
                cookieUserSrv.setUserCookie('guest');
                Data.toast(results);
                $state.go('home');
            });
        }
    }]);
