/*
    Autor = Javier Cancela Torres
    Fecha = 09/06/2016
    Licencia = GPL v3
    Versión = 1.0
    Descripción = Notijuegos, tu página de videojuegos
    Copyright (C) 2015  Javier Cancela Torres
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/* Directives */
var postDirectives = angular.module('postDirectives', []);
//directiva para hacer el scrollspy de los filtros
postDirectives.directive('sidebarAffix', ['$document', function($document) {
    return {
        link: function(scope, element, attr, window) {

            /* activate sidebar */
            element.affix({
                offset: {
                    top: 10,
                }
            });

            /* activate scrollspy menu */
            var body = angular.element(document.body);
            var navHeight = angular.element('.navbar').outerHeight(true) + 10;

            body.scrollspy({
                target: '#leftCol',
                offset: navHeight
            });
        }
    };
}])
//directiva para mostrar nota de analisis
.directive('mostrarNota', ['$document', function($document) {
    return {
        scope: {
            nota: "="
        },
        link: function(scope, element, attr) {
            scope.$watch('nota', updateNote);

            function updateNote(newValue) {
                if (newValue) {
                    element.circliful({
                        animationStep: 5,
                        foregroundBorderWidth: 5,
                        backgroundBorderWidth: 15,
                        percent: newValue
                    });
                }
            }

        }
    };
}])
//directiva para hacer el focus
.directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }
})
//directiva para comparar las passwords
.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {

                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel);

                //get the value of the other password
                var e2 = scope.$eval(attrs.passwordMatch);
                if(e2!=null)
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {

                //set the form control to valid if both
                //passwords are the same, else invalid
                control.$setValidity("passwordNoMatch", n);
            });
        }
    };
}]);
