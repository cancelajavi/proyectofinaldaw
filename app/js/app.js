/*
    Autor = Javier Cancela Torres
    Fecha = 09/06/2016
    Licencia = GPL v3
    Versión = 1.0
    Descripción = Notijuegos, tu página de videojuegos
    Copyright (C) 2015  Javier Cancela Torres
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/* App Module */
//Cargamos la aplicacion de Angular con los modulos que deseamos
var notijuegosApp = angular.module('notijuegosApp', [
  'ui.router',
  'notiJuegosControllers',
  'notijuegosServices',
  'postFilters',
  'postDirectives',
  'ngMessages',
  'ngRoute',
  'toaster',
  'ngTouch',
  'ngAnimate',
  'ngCookies'
]);
//Routing
notijuegosApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            views: {
              '@': {
                templateUrl: 'templates/main.html'
              },
              'header@home': {
                templateUrl: 'templates/header.html',
                controller: 'HeaderCtrl'
              },
              'content@home': {
                templateUrl: 'templates/post-list.html',
                controller: 'PostListCtrl'
              },
              'footer@home': {
                templateUrl: 'templates/footer.html'
              }
            }
        })
        .state('home.login', {
          parent: 'home',
          url: 'login',
          views: {
            'content@home': {
              templateUrl: 'templates/login.html',
              controller: 'authCtrl',

            }
          }
        })
        .state('home.logout', {
          parent: 'home',
          url: 'login',
          views: {
            'content@home': {
              templateUrl: 'templates/login.html',
              controller: 'logoutCtrl'
            }
          }
        })
        .state('home.signup', {
          parent: 'home',
          url: 'signup',
          views: {
            'content@home': {
              templateUrl: 'templates/signup.html',
              controller: 'authCtrl'
            }
          }
        })
        .state('home.dashboard', {
          parent: 'home',
          url: 'dashboard',
          views: {
            'content@home': {
              templateUrl: 'templates/dashboard.html',
              controller: 'authCtrl'
            }
          }
        })
        .state('post', {
          parent: 'home'
        })
        .state('post.Noticias', {
          parent: 'post',
          url: 'articulos/noticias/:postUrl',
          views: {
            'content@home': {
              templateUrl: 'templates/post-details-new.html',
              controller: 'NewsDetailsCtrl'
            }
          }
        })
        .state('post.Analisis', {
          parent: 'post',
          url: 'articulos/analisis/:postUrl',
          views: {
            'content@home': {
              templateUrl: 'templates/post-details-review.html',
              controller: 'ReviewsDetailsCtrl'
            }
          }
        })
        .state('home.privacity', {
          parent: 'home',
          url: 'privacity',
          views: {
            'content@home': {
              templateUrl: 'templates/privacity.html'
            }
          }
        })
        .state('home.legal', {
          parent: 'home',
          url: 'legal',
          views: {
            'content@home': {
              templateUrl: 'templates/legal.html'
            }
          }
        })
        .state('home.cookies', {
          parent: 'home',
          url: 'cookies',
          views: {
            'content@home': {
              templateUrl: 'templates/cookies.html'
            }
          }
        });
})
//Funcion que se ejecuta al cargar la página
.run(['cookieUserSrv', 'userSrv', function (cookieUserSrv, userSrv) {
  //Asociamos a nuestro objeto user las cookies
    if(cookieUserSrv.getUserCookie()&&cookieUserSrv.getUserCookie()!='guest'){
      userSrv.setUser(JSON.parse(cookieUserSrv.getUserCookie()));
    }
  }]);
