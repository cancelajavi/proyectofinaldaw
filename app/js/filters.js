/*
    Autor = Javier Cancela Torres
    Fecha = 09/06/2016
    Licencia = GPL v3
    Versión = 1.0
    Descripción = Notijuegos, tu página de videojuegos
    Copyright (C) 2015  Javier Cancela Torres
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/* Filters */

var postFilters = angular.module('postFilters', []);
//filtro para tratar texto como html
postFilters.filter("convertToHtml", ['$sce', function($sce) {
    return function(htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }
}]);
//convertir milisegundos a una fecha
postFilters.filter("convertToDate", function() {
    return function(StringMilliseconds) {
        var milliseconds = parseFloat(StringMilliseconds);
        var startDate = new Date(milliseconds);
        var finalDate = startDate.getDate() + "/" + (startDate.getMonth() + 1) + "/" + startDate.getFullYear();
        return finalDate;
    }
});
