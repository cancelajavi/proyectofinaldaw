/*
    Autor = Javier Cancela Torres
    Fecha = 09/06/2016
    Licencia = GPL v3
    Versión = 1.0
    Descripción = Notijuegos, tu página de videojuegos
    Copyright (C) 2015  Javier Cancela Torres
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

/* Services */

var notijuegosServices = angular.module('notijuegosServices', ['ngResource']);
//Servicio para los artículos
notijuegosServices.factory('notijuegosService', ['$resource',
  function($resource){
    return $resource('../api/v1/listarPosts.php', {}, {
    });
  }])
  //Servicio para los usuarios
.factory("Data", ['$http', 'toaster',
      function ($http, toaster) { // This service connects to our REST API

          var serviceBase = '../api/v1/';

          var obj = {};
          obj.toast = function (data) {
              toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
          }
          obj.get = function (q) {
              return $http.get(serviceBase + q).then(function (results) {
                  return results.data;
              });
          };
          obj.post = function (q, object) {
              return $http.post(serviceBase + q, object).then(function (results) {
                  return results.data;
              });
          };
          obj.put = function (q, object) {
              return $http.put(serviceBase + q, object).then(function (results) {
                  return results.data;
              });
          };
          obj.delete = function (q) {
              return $http.delete(serviceBase + q).then(function (results) {
                  return results.data;
              });
          };

          return obj;
  }])
  //servicio para crear nuestro objeto usuario
  .factory("userSrv", function () {

          var user = {};

          var factory = {
              getUser: getUser,
              setUser: setUser
          };

         return factory;

         function getUser() {
           return user;
         }

         function setUser(userLogged) {
             user = userLogged;
         }
    })
    //servicio para crear las cookies
    .factory("cookieUserSrv", ["$cookies", function ($cookies) {

            var factory = {
                getUserCookie: getUserCookie,
                setUserCookie: setUserCookie
            };

           return factory;

           function getUserCookie() {
             return $cookies.get('user');
           }

           function setUserCookie(userLogged) {
             $cookies.put('user', JSON.stringify(userLogged));
             if($cookies.get('user')){
               userLogged = JSON.parse($cookies.get('user'));
             }
           }
      }]);
