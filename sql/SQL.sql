-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.17 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para notijuegosdb
CREATE DATABASE IF NOT EXISTS `notijuegosdb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `notijuegosdb`;


-- Volcando estructura para tabla notijuegosdb.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `info` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla notijuegosdb.categories: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category_name`, `info`) VALUES
	(1, 'Analisis', 'analisis de notijuegos'),
	(2, 'Noticias', 'noticias de notijuegos');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;


-- Volcando estructura para tabla notijuegosdb.images
CREATE TABLE IF NOT EXISTS `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) COLLATE utf8_bin NOT NULL,
  `alt` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla notijuegosdb.images: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`image_id`, `image_url`, `alt`) VALUES
	(1, 'rl-1.jpg', 'Rocket-League-Destacada'),
	(2, 'portal-knights-1.jpg', 'portal-knights-destacada'),
	(3, 'Razer-Overwatch.jpg', 'razer-overwatch'),
	(4, 'Overwatch-Destacada1.jpg', 'Overwatch-Destacada');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;


-- Volcando estructura para tabla notijuegosdb.platforms
CREATE TABLE IF NOT EXISTS `platforms` (
  `platform_id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `info` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`platform_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla notijuegosdb.platforms: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
INSERT INTO `platforms` (`platform_id`, `platform_name`, `info`) VALUES
	(1, 'PS4', 'ps4'),
	(2, 'PC', 'pc'),
	(3, 'WiiU', 'wiiu'),
	(4, '3DS', '3ds');
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;


-- Volcando estructura para tabla notijuegosdb.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_bin DEFAULT 'Titulo predeterminado',
  `subtitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `img_principal` int(11) DEFAULT NULL,
  `extract` varchar(255) COLLATE utf8_bin DEFAULT 'Subtitulo predeterminado',
  `date` bigint(20) NOT NULL,
  `content` longtext COLLATE utf8_bin,
  `author` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `platform` int(11) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `post_url` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `FK_posts_images` (`img_principal`),
  KEY `FK_posts_categories` (`category`),
  KEY `FK_posts_platforms` (`platform`),
  KEY `FK_posts_users` (`author`),
  CONSTRAINT `FK_posts_categories` FOREIGN KEY (`category`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `FK_posts_images` FOREIGN KEY (`img_principal`) REFERENCES `images` (`image_id`),
  CONSTRAINT `FK_posts_platforms` FOREIGN KEY (`platform`) REFERENCES `platforms` (`platform_id`),
  CONSTRAINT `FK_posts_users` FOREIGN KEY (`author`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla notijuegosdb.posts: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`post_id`, `title`, `subtitle`, `img_principal`, `extract`, `date`, `content`, `author`, `category`, `platform`, `nota`, `post_url`) VALUES
	(1, 'Rocket League: Collector’s Edition llega a PS4 y Xbox One', 'Rocket League: Collector’s Edition llegará en formato físico a PS4 y Xbox One el 24 de junio. Traerá tres DLC, cuatro vehículos nuevos y una litografía.', 1, 'Rocket League: Collector’s Edition llegará en formato físico a PS4 y Xbox One el 24 de junio. Traerá tres DLC, cuatro vehículos nuevos y una litografía.', 1463730535679, 'Hoy, 505 Games y la desarrolladora Psyonix han anunciado el <strong>tráiler</strong> de la edición física de <a href="https://twitter.com/rocketleague">Rocket League</a><strong> para consolas</strong>. El nombre que recibirá es <strong>Rocket League: Collector’s Edition</strong>. Además del juego base, esta edición contiene <strong>tres packs de DLC</strong> (Supersonic Fury, Revenge of the Battle-Cars y Chaos Run), acceso anticipado a <strong>cuatro vehículos nuevos</strong> que más tarde se podrán comprar por separado y una <strong>litografía</strong> exclusiva. Rocket League: Collector’s Edition aparecerá en formato físico para <strong>PS4 y Xbox One el 24 de junio</strong> de 2016. Costará <strong>29,99 €</strong>.</p>', 1, 2, 2, NULL, 'rocket-league-collector-noticia'),
	(2, 'Análisis Portal Knights', 'Con esta premisa se inicia Portal Knights, un juego con una vertiente RPG muy cuidada donde tendremos que activar los 44 mundos del multiverso.', 2, 'Con esta premisa se inicia Portal Knights, un juego con una vertiente RPG muy cuidada donde tendremos que activar los 44 mundos del multiverso.', 1463730535679, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut augue purus, dictum eget ligula eget, viverra posuere massa. Duis ullamcorper diam ut urna ornare, quis auctor risus euismod. Quisque placerat consequat est eu luctus. Sed non quam arcu. Mauris interdum mauris urna, eget molestie diam aliquet in. Donec et arcu gravida tortor condimentum convallis sit amet nec elit. Praesent lorem felis, luctus et ante quis, varius condimentum mi. Ut non porta ligula. Praesent pellentesque feugiat sodales. Nam et justo porta, aliquet risus ut, consectetur elit. Donec vulputate nibh eu lorem dapibus, eu rhoncus sapien laoreet. Vestibulum ullamcorper tortor at arcu tempus, sed aliquet magna eleifend. Etiam malesuada erat sed arcu posuere aliquet. Vivamus auctor maximus erat, ut sodales ligula ultricies dignissim.', 1, 1, 2, 78, 'analisis-portal-knights'),
	(3, 'Razer lanza su línea de periféricos de Overwatch', 'Ya están disponibles para reserva los nuevos periféricos temáticos de Overwatch de la marca Razer.', 3, 'Ya están disponibles para reserva los nuevos periféricos temáticos de Overwatch de la marca Razer.', 1463730511111, 'A escasos días del lanzamiento oficial de <strong>Overwatch, Razer,</strong> una de las mayores empresas de periféricos para gaming, nos presenta <strong>sus nuevos periféricos inspirados por el juego</strong>, los cuales gozarán de funciones estéticas que tienen relación directa con lo que esté pasando en la pantalla mientras juguemos a Overwatch.\r\n<p>Esta nueva línea la forman los auriculares<strong> Razer ManO’War Tournament Edition gaming headset</strong>, el teclado <strong>BlackWidow Chroma</strong>, el ratón<strong> DeathAdder Chroma</strong> y la alfombrilla<strong> Goliathus Extended</strong>. Todos ellos creados para los más fanáticos del nuevo juego de Blizzard.</p>\r\n<p>Los auriculares Overwatch Razer ManO’War TE, los cuales estarán en las tiendas más cercanas a partir del mes de junio gozan de un <strong>micrófono retráctil</strong>, altavoces de 50mm y tienen un diseño optimizado para aislar los ruidos externos. Su jack de conexión de 3.5 mm y su adaptador splitter harán que sean compatibles con muchas plataformas distintas. Podremos reservar este headset por un precio recomendado de 119,99€.</p>\r\n<p>Quienes quieran estrenar teclado nuevo podrán hacerse con el<strong> Razer Blackwidow Chroma Overwatch</strong>. Este nuevo modelo de teclado cuenta con<strong> señales de iluminación que harán referencia a cada uno de los personajes</strong>; podremos usar sus <strong>opciones de luces para ver los cooldowns de nuestras habilidades</strong> en tiempo real. Todo esto, además, personalizado para tener distintas opciones de iluminación por cada héroe. El Blackwidow Chroma Overwatch estará disponible por<strong> 209,99€</strong>; además de eso podremos obtener el <strong>Deathadder Chroma Overwatch</strong> por otros <strong>89,99€</strong>.</p>\r\n<blockquote><p>“Estamos muy orgullosos de dar a los jugadores de Overwatch una experiencia inmersiva en el juego con un audio de calidad y el hardware ideal personalizado con efectos de iluminación gracias a nuestra tecnología Chroma [&#8230;] Overwatch es uno de los juegos más dinámicos y visualmente sorprendentes que hemos disfrutado en estos últimos años, por lo que estos periféricos que hemos diseñado junto con Blizzard añaden una nueva dimensión a la diversión del juego.”</p>\r\n<p>&#8211;  Min-Liang Tan, Cofundador y CEO de Razer</p></blockquote>\r\n<p>Para una información más detallada de todos los periféricos, podéis dirigiros a <a href="http://www.razerzone.com/licensed-and-team-peripherals/overwatch">este enlace</a>, donde podréis encontrar las fichas técnicas de toda la línea de productos <strong>Overwatch</strong> de Razer. Además de eso, si <strong>ya queréis irlos reservando</strong>, podéis dirigiros a <a href="http://www.razerzone.com/store/chroma-offer">la página de Razerzone</a> o a <a href="http://gear.blizzard.com/index.php/default/overwatch-gaming-peripherals.html">la web de Blizzard</a>.</p>\r\n', 2, 2, 1, NULL, 'razer-lanza-su-linea-de-perifericos-de-overwatch'),
	(4, 'Nuevo corto de Overwatch: Héroe', 'A pocos días de la salida de Overwatch, Blizzard nos presenta un nuevo corto animado del juego.', 4, 'A pocos días de la salida de Overwatch, Blizzard nos presenta un nuevo corto animado del juego.', 1463988447702, 'Yo no empecé esta guerra. . .pero pienso acabarla de una vez por todas” </span></i><span lang="ES">Con estas palabras se nos presenta el último corto de esta temporada, <strong>Héroe</strong>. </span>\r\n<p>En este nuevo corto de <strong>Overwatch,</strong> podremos ver cómo el <strong>Soldado: 76</strong> se adentra en una misión que lo llevará a el Dorado; en el cual investigará a la banda de Los Muertos, una organización criminal.</p>\r\n<p>Nada podía ser tan fácil para nuestro soldado, pues tendrá que vérselas con una complicación completamente inesperada.</p>\r\n<p>Tras esto, recordaros a los pocos que no lo tengáis marcado en vuestro calendario que Overwatch sale a la venta pasado mañana, el día<strong> 24 de mayo</strong>.', 2, 2, 1, NULL, 'overwatch-corto-heroe');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;


-- Volcando estructura para tabla notijuegosdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `privilege` enum('user','writer','editor','admin') COLLATE utf8_bin NOT NULL DEFAULT 'user',
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla notijuegosdb.users: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`uid`, `username`, `user_name`, `surname`, `email`, `privilege`, `password`, `created`) VALUES
	(1, 'xchappi', 'javier', 'cancela', 'cancelajavi@gmail.com', 'admin', 'as$Dfghj$2a$2a$10$477f7567571278c17ebdees5xCunwKISQaG8zkKhvfE5dYem5sTey', '0000-00-00 00:00:00'),
	(2, 'pepito', 'pepe', 'castro', 'pepe@pepe.com', 'writer', 'as$Dfghj$2a$2a$10$477f7567571278c17ebdees5xCunwKISQaG8zkKhvfE5dYem5sTey', '0000-00-00 00:00:00'),
	(10, 'Test 1', 'Test 1', 'Test 1', 'test1@test.com', 'user', '$2a$10$4c7e1df9b730dc495ea5aucmMppvevVHc56mXTiepOA6rPEj41GX.', '2016-06-12 23:22:06'),
	(11, 'Test 2', 'Test 2', 'Test 2', 'test2@test.com', 'user', '$2a$10$6f012c78160104262ced9eq7yHKkpjYgF71gpvbx5s1znBh1/kGRi', '2016-06-12 23:22:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
