<?php
require_once 'medoo.php';
  $database = new medoo([
      'database_type' => 'mysql',
      'database_name' => 'notijuegosdb',
      'server' => 'localhost',
      'username' => 'root',
      'password' => '',
      'charset' => 'utf8'
  ]);
  if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
  }else{
    $accion = $_GET['accion'];
  }
if($accion == "pedirPosts"){
  pedirArticulo($database);
}
else if ($accion == "detallesPost"){
  detallesArticulo($database);
}

function pedirArticulo($database){
  $postsData = $database->query("select post_id, title, extract, images.image_url, users.username, date, categories.category_name, platforms.platform_name, post_url from posts, users, images, categories, platforms where (users.username = (select username from users where uid = posts.author) and images.image_url = (select image_url from images where image_id = posts.img_principal)and categories.category_name = (select category_name from categories where category_id = posts.category) and platforms.platform_name = (select platform_name from platforms where platform_id = posts.platform));")->fetchAll();
  $posts = new stdClass();
  $posts->postsData = $postsData;
  echo json_encode($posts);
}

function detallesArticulo($database){

  $postUrl = $_GET["postUrl"];
  $postsData = $database->query("select title, subtitle, content, images.image_url, users.username, date, platforms.platform_name, nota from posts, users, images, platforms where (users.username = (select username from users where uid = posts.author) and images.image_url = (select image_url from images where image_id = posts.img_principal) and platforms.platform_name = (select platform_name from platforms where platform_id = posts.platform) and post_url = $postUrl);")->fetchAll();
  $detallesPost = new stdClass();
  $detallesPost->post = $postsData;
  echo json_encode($detallesPost);
}
 ?>
